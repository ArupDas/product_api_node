const CommonHelper = require("../util/common-helper");
const SqlQueryBuilder = require("../util/sql-query-builder");

class UserModel {
   /**
  * user signup
  * @param {*} params
  */
    signUp(params) {
        return new Promise((resolve, reject) => {
          try {
            const hashPassword = params.password ? CommonHelper.hashingPassword(params.password) : null;
            const newParams = [];
            newParams.push(SqlQueryBuilder.SqlParameter(params.userName));
            newParams.push(SqlQueryBuilder.SqlParameter(hashPassword));
            newParams.push(SqlQueryBuilder.SqlParameter(params.roleId));
            SqlQueryBuilder.Execute(`CALL user_register(?,?,?)`, newParams).then((dbRes) => {
              if (dbRes && dbRes[0] && dbRes[0][0] && dbRes[0][0]["@full_error"]) {
                reject(dbRes[0][0]["@full_error"]);
              } else if (dbRes && dbRes[0][0]) {
                resolve(dbRes[0][0]);
              } else if (dbRes && dbRes[0]) {
                resolve(dbRes[0]);
              } else {
                reject(null);
              }
            }).catch((err) => {
              reject(err);
            });
    
          } catch (e) {
            reject(e);
          }
        });
      }

    /**
  * user Login
  * @param {*} params
  */
  login(params) {
    return new Promise((resolve, reject) => {
      try {
        const newParams = [];
        newParams.push(SqlQueryBuilder.SqlParameter(params.userName));
        SqlQueryBuilder.Execute(`CALL user_login(?)`, newParams).then((dbRes) => {
          if (dbRes && dbRes[0] && dbRes[0][0] && dbRes[0][0]["@full_error"]) {
            reject(dbRes[0][0]["@full_error"]);
          } else if (dbRes && dbRes[0][0]) {
            resolve(dbRes[0][0]);
          } else if (dbRes && dbRes[0]) {
            resolve(dbRes[0]);
          } else {
            reject(null);
          }
        }).catch((err) => {
          reject(err);
        });
      } catch (e) {
        reject(e);
      }
    });
  }
}
module.exports = new UserModel();
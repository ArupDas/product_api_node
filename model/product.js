const CommonHelper = require("../util/common-helper");
const SqlQueryBuilder = require("../util/sql-query-builder");

class ProductModel {
  

    /**
  * get product list
  * @param {*} params
  */
    getProducts() {
    return new Promise((resolve, reject) => {
      try {
        const newParams = [];
        SqlQueryBuilder.Execute(`CALL get_product_list()`, newParams).then((dbRes) => {
          if (dbRes && dbRes[0] && dbRes[0][0] && dbRes[0][0]["@full_error"]) {
            reject(dbRes[0][0]["@full_error"]);
          }  else if (dbRes && dbRes[0]) {
            resolve(dbRes[0]);
          } else {
            reject(null);
          }
        }).catch((err) => {
          reject(err);
        });
      } catch (e) {
        reject(e);
      }
    });
  }

   /**
  * get product detail
  * @param {*} params
  */
    getProductDetail(params) {
      return new Promise((resolve, reject) => {
        try {
          const newParams = [];
          newParams.push(SqlQueryBuilder.SqlParameter(params.productId));
          SqlQueryBuilder.Execute(`CALL get_product_detail(?)`, newParams).then((dbRes) => {
            if (dbRes && dbRes[0] && dbRes[0][0] && dbRes[0][0]["@full_error"]) {
              reject(dbRes[0][0]["@full_error"]);
            } else if (dbRes && dbRes[0][0]) {
              resolve(dbRes[0][0]);
            } else if (dbRes && dbRes[0]) {
              resolve(dbRes[0]);
            } else {
              reject(null);
            }
          }).catch((err) => {
            reject(err);
          });
        } catch (e) {
          reject(e);
        }
      });
    }

   /**
  * post Products
  * @param {*} params
  */
    postProducts(params) {
        return new Promise((resolve, reject) => {
          try {
            const newParams = [];
            newParams.push(SqlQueryBuilder.SqlParameter(params.productName));
            newParams.push(SqlQueryBuilder.SqlParameter(params.imgUrl));
            SqlQueryBuilder.Execute(`CALL post_product(?,?)`, newParams).then((dbRes) => {
              if (dbRes && dbRes[0] && dbRes[0][0] && dbRes[0][0]["@full_error"]) {
                reject(dbRes[0][0]["@full_error"]);
              } else if (dbRes && dbRes[0][0]) {
                resolve(dbRes[0][0]);
              } else if (dbRes && dbRes[0]) {
                resolve(dbRes[0]);
              } else {
                reject(null);
              }
            }).catch((err) => {
              reject(err);
            });
          } catch (e) {
            reject(e);
          }
        });
      }

   /**
  * update Products
  * @param {*} params
  */
    updateProducts(params) {
      return new Promise((resolve, reject) => {
        try {
          const newParams = [];
          newParams.push(SqlQueryBuilder.SqlParameter(params.productName));
          newParams.push(SqlQueryBuilder.SqlParameter(params.imgUrl));
          newParams.push(SqlQueryBuilder.SqlParameter(Number(params.productId)));
          SqlQueryBuilder.Execute(`CALL update_product(?,?,?)`, newParams).then((dbRes) => {
            if (dbRes && dbRes[0] && dbRes[0][0] && dbRes[0][0]["@full_error"]) {
              reject(dbRes[0][0]["@full_error"]);
            } else if (dbRes && dbRes[0][0]) {
              resolve(dbRes[0][0]);
            } else if (dbRes && dbRes[0]) {
              resolve(dbRes[0]);
            } else {
              reject(null);
            }
          }).catch((err) => {
            reject(err);
          });
        } catch (e) {
          reject(e);
        }
      });
    }

     /**
  * delete product
  * @param {*} params
  */
      deleteProducts(params) {
        return new Promise((resolve, reject) => {
          try {
            const newParams = [];
            newParams.push(SqlQueryBuilder.SqlParameter(params.productId));
            SqlQueryBuilder.Execute(`CALL delete_product(?)`, newParams).then((dbRes) => {
              if (dbRes && dbRes[0] && dbRes[0][0] && dbRes[0][0]["@full_error"]) {
                reject(dbRes[0][0]["@full_error"]);
              } else if (dbRes && dbRes[0][0]) {
                resolve(dbRes[0][0]);
              } else if (dbRes && dbRes[0]) {
                resolve(dbRes[0]);
              } else {
                reject(null);
              }
            }).catch((err) => {
              reject(err);
            });
          } catch (e) {
            reject(e);
          }
        });
      }

}
module.exports = new ProductModel();
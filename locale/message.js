'use strict';
module.exports = {
  'avail_lang': { 'en': 'en' },
  'en': {   
    ALREADY: 'Already exist',
    BAD_REQUEST: 'Bad request',
    CREATED_SUCCESS:'Created successfully',    
    DELETED_SUCCESS: 'Deleted successfully',
    TOKEN_EXPIRED: 'Token expired',

    FAILED_TO_AUTHENTICATE: 'Failed to authenticate',

    INVALID_USER: 'User does not exist',
    INVALID_TOKEN: 'Invalid Token!',
    IS_NOT_ALLOWED: 'is not allowed',   
    
    IS_REQUIRED: 'is required',
    NO_RECORD: 'No record found',
    RECORD_NOT_FOUND:'Record not found',
    SHOULD_BE_STRING: 'should be string',
    SHOULD_BE_NUMBER: 'should be number',
    MUST_BE_A_BOOLEAN: 'must be a boolean',
    SHOULD_BE_DATE: 'should be date',
    MUST_BE_LESS_THAN_OR_EQUAL_TO: 'must be less than or equal to',
    MUST_BE_LARGER_THAN_OR_EQUAL_TO: 'must be larger than or equal to character',
    MUST_BE_ALPHANUM:'Password must contain a-z, A-Z and 0-9',
    IS_NOT_ALLOWED_TO_BE_EMPTY: 'is not allowed to be empty',
    ALLOW_ONLY_ALPHA_NUMERIC: 'must only contain alpha-numeric characters',
    MUST_BE_A_VALID_EMAIL: 'must be a valid email',
    SOMETHING_WENT_WRONG:'Something went wrong',
    UPDATE_SUCCESSS: 'Updated successfully',
    USER_EXIST:'User already exists',
    PRODUCT_DELETE:'Product deleted successfully',
    PRODUCT_UPDATE:'Product updated successfully',
    PRODUCT_ADDED:'Product added successfully',
    PRODUCT_SEND:'Products sent successfully',
    ENDPOINT_UNAUTH:'Not authorized to access endpoint',
    USER_NOT_EXISTS:'User not exist',
    INVALID_PASSWORD:'Invalid password',
    USER_CREATED_SUCCESS:'User created successfully',  
    ACCESS_DENIED:'Access Denied'
    
  }
}

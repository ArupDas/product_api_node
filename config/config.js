"use strict";

const env = "dev"; //process.env.NODE_ENV ? process.env.NODE_ENV : "dev"; // 'dev' or 'prod'

const dev = {
  APP: {
    PORT: process.env.PORT || 3000,
    HTTP: process.env.HOST || "http://localhost",
  },
  
  DB_CONFIG: {
    USER: "root",
    PASSWORD: "password",
    HOST: "localhost", // You can use 'localhost\\instance' to connect to named instance
    DATABASE: "one_test",
    CONNECTION_TIME_OUT: 300000,
    CONNECTION_LIMIT: 100,
    PORT: 3306
    },
  
};

const config = { dev };
module.exports = config[env];

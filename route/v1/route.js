'use strict';
const express = require('express');
const router = express.Router();
const TokenMiddleware = require('../../middleware/token-middleware');
const UsersController = require('../../controller/users');
const ProductController = require('../../controller/product');





router.post('/signup', UsersController.signUp);
router.post('/login', UsersController.login);

// product
router.get('/product',TokenMiddleware.Authorize,ProductController.getProducts);
router.get('/product/:productId',TokenMiddleware.Authorize,ProductController.getProductDetail);
router.post('/product',TokenMiddleware.Authorize,ProductController.postProducts);
router.put('/product',TokenMiddleware.Authorize,ProductController.updateProducts);
router.delete('/product/:productId',TokenMiddleware.Authorize,ProductController.deleteProducts);


module.exports = router;
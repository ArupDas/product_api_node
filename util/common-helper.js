const promise = require('promise');
const Constant = require('../config/constant');
const Config = require('../config/config');
const crypto = require('crypto');
const bcrypt = require('bcryptjs');

class CustomFunc {


	/**
	* Method: formatJoiError
	* Purpose: Formate array error message to single error string 
	* @param {*} error 
	* @param {*} LangMsg 
	* @response {*} single error string 
	*/
	formatJoiError(error, LangMsg) {
		try {
			let errorType = error.details[0]["type"];
			var msg = "";
			switch (errorType) {
				case "object.unknown":
					if (LangMsg) {
						msg = error.details[0]['context']["label"].toString() + " " + LangMsg.IS_NOT_ALLOWED;
						msg = msg.trim();
					} else {
						msg = error.details[0]['message'].toString();
						msg = msg.trim();
					}
					break;
				case "any.allowOnly":
					msg = error.details[0]['message'].replace(error.details[0]['context']["label"], '').toString();
					msg = error.details[0]['context']["label"].toString().trim() + " " + msg;
					msg = msg.trim();
					break;
				case "any.empty":
					msg = error.details[0]['message'].replace(error.details[0]['context']["label"], '').toString();
					msg = error.details[0]['context']["label"].toString().trim() + " " + msg;
					msg = msg.trim();
					break;
				case "string.empty":
					msg = error.details[0]['message'].replace(error.details[0]['context']["label"], '').toString();
					msg = error.details[0]['context']["label"].toString().trim() + " " + msg;
					msg = msg.trim();
					break;
				case "any.required":
					msg = error.details[0]['message'].replace(error.details[0]['context']["label"], '').toString();
					msg = error.details[0]['context']["label"].toString().trim() + " " + msg;
					msg = msg.trim();
					break;
				case "object.base":
					msg = error.details[0]['message'].replace(error.details[0]['context']["label"], '').toString();
					msg = error.details[0]['context']["label"].toString().trim() + " " + msg;
					msg = msg.trim();
					break;
				case "number.base":
					msg = error.details[0]['message'].replace(error.details[0]['context']["label"], '').toString();
					msg = error.details[0]['context']["label"].toString().trim() + " " + msg;
					msg = msg.trim();
					break;
				case "string.base":
					msg = error.details[0]['message'].replace(error.details[0]['context']["label"], '').toString();
					msg = error.details[0]['context']["label"].toString().trim() + " " + msg;
					msg = msg.trim();
					break;
				case "string.alphanum":
					msg = error.details[0]['message'].replace(error.details[0]['context']["label"], '').toString();
					msg = error.details[0]['context']["label"].toString().trim() + " " + msg;
					msg = msg.trim();
					break;
				case "string.max":
					msg = error.details[0]['message'].replace(error.details[0]['context']["label"], '').toString();
					msg = error.details[0]['context']["label"].toString().trim() + " " + msg + " " + error.details[0]['context']["limit"].toString().trim();
					msg = msg.trim();
					break;
				case "string.min":
					msg = error.details[0]['message'].replace(error.details[0]['context']["label"], '').toString();
					msg = error.details[0]['context']["label"].toString().trim() + " " + msg + " " + error.details[0]['context']["limit"].toString().trim();
					msg = msg.trim();
					break;
				case "string.email":
					msg = error.details[0]['message'].replace(error.details[0]['context']["label"], '').toString();
					msg = error.details[0]['context']["label"].toString().trim() + " " + msg;
					msg = msg.trim();
					break;
				case "boolean.base":
					msg = error.details[0]['message'].replace(error.details[0]['context']["label"], '').toString();
					msg = error.details[0]['context']["label"].toString().trim() + " " + msg;
					msg = msg.trim();
					break;
				case "number.max":
					msg = error.details[0]['message'].replace(error.details[0]['context']["label"], '').toString();
					msg = error.details[0]['context']["label"].toString().trim() + " " + msg + " " + error.details[0]['context']["limit"].toString().trim();
					msg = msg.trim();
					break;
				case "number.min":
					msg = error.details[0]['message'].replace(error.details[0]['context']["label"], '').toString();
					msg = error.details[0]['context']["label"].toString().trim() + " " + msg + " " + error.details[0]['context']["limit"].toString().trim();
					msg = msg.trim();
					break;
				case "array.max":
					msg = error.details[0]['message'].replace(error.details[0]['context']["label"], '').toString();
					msg = error.details[0]['context']["label"].toString().trim() + " " + msg + " " + error.details[0]['context']["limit"].toString().trim();
					msg = msg.trim();
					break;
				default:
					msg = error.details[0]['message'].replace(error.details[0]['context']["label"], '').toString();
					msg = error.details[0]['context']["label"].toString() + msg;
					msg = msg.trim();
					//  msg = error.details[0]['message'].replace(error.details[0]['context']["label"], '').toString(); msg = msg.substring(2).trim();
					break;
			}
			// msg=msg.replace(msg[0],msg[0].toUpperCase());
			return msg;
		} catch (e) {
			return e.message;
		}
	}

	/**
	* Method: aesEncript
	* Purpose: encript string value
	* @param {*} planText 
	* @response {*} encript string 
	*/
	aesEncript(planText) {
		try {
			//iv needs to be 16bytes long, key is 32bytes. And we changed createCipher to createCipheriv.
			const iv = Buffer.from(Constant.AES_CONFIG.SECRETIV_KEY);
			const key = Buffer.from(Constant.AES_CONFIG.SECRET_KEY);
			const cipher = crypto.createCipheriv('aes-256-cbc', key, iv);
			const encryptedData = cipher.update(planText, 'utf8', 'hex') + cipher.final('hex');
			return encryptedData;
		} catch (e) {
			return '';
		}

	}

	/**
	* Method: aesDecript
	* Purpose: Decript string to normal string
	* @param {*} encryptedText 
	* @response {*} normal string 
	*/
	aesDecript(encryptedText) {
		try {
			//iv needs to be 16bytes long, key is 32bytes. And we changed createCipher to createCipheriv.
			encryptedText = encryptedText.toString();
			let iv = Buffer.from(Constant.AES_CONFIG.SECRETIV_KEY);
			let key = Buffer.from(Constant.AES_CONFIG.SECRET_KEY);
			let cipher = crypto.createDecipheriv('aes-256-cbc', key, iv);
			let encryptedData = cipher.update(encryptedText, 'hex', 'utf8') + cipher.final('utf8');
			return encryptedData;
		} catch (e) {
			return '';
		}
	}

	/**
	* Method: encodeBase64
	* Purpose: string value convert in base64
	* @param {*} planText 
	* @response {*} base64 string 
	*/
	encodeBase64(planText) {
		try {
			let buff = Buffer.from(planText.toString());
			let base64data = buff.toString('base64');
			return base64data;
		} catch (e) {
			return '';
		}
	}

	/**
	* Method: decodeBase64
	* Purpose: base64 to normal string
	* @param {*} base64Text 
	* @response {*} normal string 
	*/
	decodeBase64(base64Text) {
		try {
			let data = 'c3RhY2thYnVzZS5jb20=';
			let buff = Buffer.from(base64Text, 'base64');
			let text = buff.toString('ascii');
			return text;
		} catch (e) {
			return '';
		}
	}

	/**
	* Method: getHashText
	* Purpose: value convert to md5 string
	* @param {*} base64Text 
	* @response {*} md5 string 
	*/
	getHashText(text) {
		try {
			return crypto.createHash('md5').update(text).digest('hex');
		} catch (e) {
			return '';
		}
	}

	/**
	* Method: compareHashText
	* Purpose: decript to string with comapare string
	* @param {*} base64Text 
	* @response {*} bool value
	*/
	compareHashText(text, hashText) {
		try {
			let newText = crypto.createHash('md5').update(text).digest('hex');
			return newText == hashText;
		} catch (e) {
			return false;
		}
	}

	/**
	* Method: generateOTP
	* Purpose: Generate random OTP
	* @response {*} OTP number
	*/
	generateOTP() {
		try {
			return Math.floor(1000 + Math.random() * 9000);
		} catch (e) {
			return null;
		}
	}

	/**
	* Method: sendSuccess
	* Purpose: resonse formate create 
	* @param {*} res 
	* @param {*} status 
	* @param {*} response 
	* @response {*} http response
	*/
	sendSuccess(res, status,response) {
		res.status(status).json(response)
	}

	/**
	* Method: sendError
	* Purpose: error response formate  
	* @param {*} res 
	* @param {*} status 
	* @param {*} response 
	* @response {*} http response
	*/
	sendError(res, status, message) {
		res.status(status).json(message)
	}

	/**
	* Method: createGuid
	* Purpose : create guid for unique file
	*/
	createGuid() {
		return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
			var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
			return v.toString(16);
		});
	}

	

	/**
	* Method: hashingPassword
	* Purpose : hash password 
	*/
	hashingPassword(password) {
		if (password) {
			const hash = bcrypt.hashSync(password, Constant.SALT_ROUNDS);
			return hash;
		} else {
			return null;
		}
	}

	/**
	* Method: hashingCompairPassword
	* Purpose : hash compare password 
	*/
	hashingComparePassword(password, hashPassword) {
		if (password) {
			const hash = bcrypt.compareSync(password, hashPassword);			
			return hash;
		} else {
			return null;
		}
	}

}

module.exports = new CustomFunc();

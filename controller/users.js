'use strict';
const UserRequest = require('../request-schema/user-request');
const UserModel = require('../model/users');
const Constant = require('../config/constant');
const CommonHelper = require('../util/common-helper');
const JwtTokenHelper = require('../util/jwt-token-helper');
const Message = require('../locale/message');



class UserController {

     /**
  * Method: signUp
  * Purpose: This API used to user signUp
  * @param {*} req 
  * @param {*} res 
  */
  async signUp(req, res) {
    const LangMsg = Message[req.app.get("lang")];

    try {
      const input = req.body;
      const { error, value } = UserRequest.signUp(LangMsg).validate(input);
      if (error && error.details[0]) {
        return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_400_BAD_REQUEST, { 'message': CommonHelper.formatJoiError(error, LangMsg) })
      }

      const response = await UserModel.signUp(req.body).catch((err) => {
        switch (err) {
          case 'USER_EXIST':
            return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_409_CONFLICT, { 'message': LangMsg.USER_EXIST });
          default:
            return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_500_INTERNAL_SERVER_ERROR, { 'message': err });
        }
      });

      if (!response) { return; }
        return CommonHelper.sendSuccess(res, Constant.STATUS_CODE.HTTP_201_CREATED,{'message':LangMsg.USER_CREATED_SUCCESS} )
      
    } catch (e) {
      return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_500_INTERNAL_SERVER_ERROR, { 'message': e })
    }
  }

  /**
   * Method: Login
   * Purpose: This API used to log in
   * @param {*} req 
   * @param {*} res 
   */
   async login(req, res) {
    const LangMsg = Message[req.app.get("lang")];

    try {
      const input = req.body;
      const { error, value } = UserRequest.login(LangMsg).validate(input);
      if (error && error.details[0]) {
        return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_400_BAD_REQUEST, { 'message': CommonHelper.formatJoiError(error, LangMsg) })
      }

      const response = await UserModel.login(input).catch((err) => {
        switch (err) {
          case 'USER_NOT_EXISTS':
            return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_400_BAD_REQUEST, { 'message': LangMsg.USER_NOT_EXISTS });
          default:
            return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_500_INTERNAL_SERVER_ERROR, { 'message': LangMsg.SOMETHING_WENT_WRONG });
        }
      });

      if (!response) { return; }
        const hashRes = CommonHelper.hashingComparePassword(input.password, response.password);
        if (!hashRes) {
          return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_400_BAD_REQUEST, { 'message': LangMsg.INVALID_PASSWORD });
        }
        const tokens = JwtTokenHelper.generateToken({
          userName: response.username,
          roleId: response.roleId,
          permission:response.permission
        }, Constant.JWT.TOKEN_LIFE, true);
        const data = {
          "accessToken": tokens.accessToken,
        }
        return CommonHelper.sendSuccess(res, Constant.STATUS_CODE.HTTP_200_OK, data)
       
    } catch (e) {
      return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_500_INTERNAL_SERVER_ERROR, e.stack.toString())
    }
  }
}
module.exports = new UserController();
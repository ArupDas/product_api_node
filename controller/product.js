'use strict';
const ProductRequest = require('../request-schema/product-request');
const ProductModel = require('../model/product');
const Constant = require('../config/constant');
const CommonHelper = require('../util/common-helper');
const JwtTokenHelper = require('../util/jwt-token-helper');
const Message = require('../locale/message');



class ProductController {

  /**
  * Method: getProducts
  * Purpose: This API used to get the product list
  * @param {*} req 
  * @param {*} res 
  */
  async getProducts(req, res) {
    const LangMsg = Message[req.app.get("lang")];
    try {
      const response = await ProductModel.getProducts().catch((err) => {
        if (err == 'NOT_FOUND') {
            return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_404_NOT_FOUND, { 'message': LangMsg.RECORD_NOT_FOUND });
        } else {
            return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_400_BAD_REQUEST, { 'message': err })
        }
      });

      if (!response) { return; }

       else {
        return CommonHelper.sendSuccess(res, Constant.STATUS_CODE.HTTP_200_OK, response)
      }
    } catch (e) {
      return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_500_INTERNAL_SERVER_ERROR, { 'message': e })
    }
  }

   /**
  * Method: getProductDetail
  * Purpose: This API used to get the product detail
  * @param {*} req 
  * @param {*} res 
  */
    async getProductDetail(req, res) {
      const LangMsg = Message[req.app.get("lang")];
      try {
        const input = req.params;
          const { error, value } = ProductRequest.getProductDetail(LangMsg).validate(input);
          if (error && error.details[0]) {
            return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_400_BAD_REQUEST, { 'message': CommonHelper.formatJoiError(error, LangMsg) })
          }
        const response = await ProductModel.getProductDetail(input).catch((err) => {
          if (err == 'NOT_FOUND') {
              return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_404_NOT_FOUND, { 'message': LangMsg.RECORD_NOT_FOUND });
          } else {
              return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_400_BAD_REQUEST, { 'message': err })
          }
        });
  
        if (!response) { return; }
  
         else {
          return CommonHelper.sendSuccess(res, Constant.STATUS_CODE.HTTP_200_OK, response)
        }
      } catch (e) {
        return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_500_INTERNAL_SERVER_ERROR, { 'message': e })
      }
    }

   /**
  * Method: postProducts
  * Purpose: This API used to postProducts
  * @param {*} req 
  * @param {*} res 
  */
    async postProducts(req, res) {
        const LangMsg = Message[req.app.get("lang")];
    
        try {
          const input = req.body;
          const { error, value } = ProductRequest.postProducts(LangMsg).validate(input);
          if (error && error.details[0]) {
            return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_400_BAD_REQUEST, { 'message': CommonHelper.formatJoiError(error, LangMsg) })
          }
    
          const response = await ProductModel.postProducts(req.body).catch((err) => {
            if (err) {
                return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_500_INTERNAL_SERVER_ERROR, { 'message': err });
            }
          });
    
          if (!response) { return; }
            return CommonHelper.sendSuccess(res, Constant.STATUS_CODE.HTTP_201_CREATED,{'message':LangMsg.PRODUCT_ADDED} )
          
        } catch (e) {
          return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_500_INTERNAL_SERVER_ERROR, { 'message': e })
        }
      }

       /**
  * Method: updateProducts
  * Purpose: This API used to updateProducts
  * @param {*} req 
  * @param {*} res 
  */
    async updateProducts(req, res) {
      const LangMsg = Message[req.app.get("lang")];
  
      try {
        const input = req.body;
        const { error, value } = ProductRequest.updateProducts(LangMsg).validate(input);
        if (error && error.details[0]) {
          return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_400_BAD_REQUEST, { 'message': CommonHelper.formatJoiError(error, LangMsg) })
        }
  
        const response = await ProductModel.updateProducts(req.body).catch((err) => {
          if (err == 'NOT_FOUND') {
            return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_404_NOT_FOUND, { 'message': LangMsg.RECORD_NOT_FOUND });
        } else {
            return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_400_BAD_REQUEST, { 'message': err })
        }
        });
  
        if (!response) { return; }
          return CommonHelper.sendSuccess(res, Constant.STATUS_CODE.HTTP_200_OK,{'message':LangMsg.PRODUCT_UPDATE} )
        
      } catch (e) {
        return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_500_INTERNAL_SERVER_ERROR, { 'message': e })
      }
    }

     /**
  * Method: deleteProducts
  * Purpose: This API used to delete
  * @param {*} req 
  * @param {*} res 
  */
      async deleteProducts(req, res) {
        const LangMsg = Message[req.app.get("lang")];
        try {
          const input = req.params;
            const { error, value } = ProductRequest.deleteProducts(LangMsg).validate(input);
            if (error && error.details[0]) {
              return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_400_BAD_REQUEST, { 'message': CommonHelper.formatJoiError(error, LangMsg) })
            }
          const response = await ProductModel.deleteProducts(input).catch((err) => {
            if (err == 'NOT_FOUND') {
                return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_404_NOT_FOUND, { 'message': LangMsg.RECORD_NOT_FOUND });
            } else {
                return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_400_BAD_REQUEST, { 'message': err })
            }
          });
    
          if (!response) { return; }
    
           else {
            return CommonHelper.sendSuccess(res, Constant.STATUS_CODE.HTTP_200_OK, { 'message': LangMsg.PRODUCT_DELETE })
          }
        } catch (e) {
          return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_500_INTERNAL_SERVER_ERROR, { 'message': e })
        }
      }

}
module.exports = new ProductController();
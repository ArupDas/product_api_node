const Joi = require('@hapi/joi');
class UserRequest {

     /**
    * Method: Registration
    * Purpose: JOI validation
    * @param {*} LangMsg 
    */
      signUp(LangMsg) {
        return Joi.object().keys({
            userName: Joi.string().max(50).required()
                .messages({
                    "string.max": LangMsg.MUST_BE_LESS_THAN_OR_EQUAL_TO,
                    "any.required": LangMsg.IS_REQUIRED,
                    "string.empty": LangMsg.IS_NOT_ALLOWED_TO_BE_EMPTY
                }),
            password: Joi.string().max(150).required().alphanum()
                .messages({
                    "string.max": LangMsg.MUST_BE_LESS_THAN_OR_EQUAL_TO,
                    "string.alphanum": LangMsg.MUST_BE_ALPHANUM,
                    "any.required": LangMsg.IS_REQUIRED,
                    "string.empty": LangMsg.IS_NOT_ALLOWED_TO_BE_EMPTY
                }),
            roleId: Joi.number().required()
                .messages({
                    "any.required": LangMsg.IS_REQUIRED
                }),
            
        });
    } 

     /**
    * Method: Login
    * Purpose: JOI validation
    * @param {*} LangMsg 
    */
      login(LangMsg) {
        return Joi.object().keys({
            userName: Joi.string().max(50).required()
                .messages({
                    "string.max": LangMsg.MUST_BE_LESS_THAN_OR_EQUAL_TO,
                    "any.required": LangMsg.IS_REQUIRED,
                    "string.empty": LangMsg.IS_NOT_ALLOWED_TO_BE_EMPTY
                }),
            password: Joi.string().max(150).min(8).required().alphanum()
                .messages({
                    "string.alphanum": LangMsg.MUST_BE_ALPHANUM,
                    "string.max": LangMsg.MUST_BE_LESS_THAN_OR_EQUAL_TO,
                    "string.min": LangMsg.MUST_BE_LARGER_THAN_OR_EQUAL_TO,
                    "any.required": LangMsg.IS_REQUIRED,
                    "string.empty": LangMsg.IS_NOT_ALLOWED_TO_BE_EMPTY
                }),
        });
    }
}
module.exports = new UserRequest();
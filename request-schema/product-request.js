const Joi = require('@hapi/joi');
class ProductRequest {

     /**
    * Method: postProducts
    * Purpose: JOI validation
    * @param {*} LangMsg 
    */
      postProducts(LangMsg) {
        return Joi.object().keys({
            productName: Joi.string().max(50).required()
                .messages({
                    "string.max": LangMsg.MUST_BE_LESS_THAN_OR_EQUAL_TO,
                    "any.required": LangMsg.IS_REQUIRED,
                    "string.empty": LangMsg.IS_NOT_ALLOWED_TO_BE_EMPTY
                }),
            imgUrl: Joi.string().required()
                .messages({
                    "any.required": LangMsg.IS_REQUIRED,
                    "string.empty": LangMsg.IS_NOT_ALLOWED_TO_BE_EMPTY
                }),
            
        });
    } 

     /**
    * Method: updateProducts
    * Purpose: JOI validation
    * @param {*} LangMsg 
    */
      updateProducts(LangMsg) {
        return Joi.object().keys({
            productName: Joi.string().max(50).required()
                .messages({
                    "string.max": LangMsg.MUST_BE_LESS_THAN_OR_EQUAL_TO,
                    "string.empty": LangMsg.IS_NOT_ALLOWED_TO_BE_EMPTY,
                    "any.required": LangMsg.IS_REQUIRED,
                }),
            imgUrl: Joi.string().required()
                .messages({
                    "string.empty": LangMsg.IS_NOT_ALLOWED_TO_BE_EMPTY,
                    "any.required": LangMsg.IS_REQUIRED,
                }),
            productId: Joi.string().required()
                .messages({
                    "any.required": LangMsg.IS_REQUIRED,
                    "string.empty": LangMsg.IS_NOT_ALLOWED_TO_BE_EMPTY
                }),
            
        });
    } 

     /**
    * Method: getProductDetail
    * Purpose: JOI validation
    * @param {*} LangMsg 
    */
      getProductDetail(LangMsg) {
        return Joi.object().keys({
            productId: Joi.string().required()
                .messages({
                    "any.required": LangMsg.IS_REQUIRED,
                    "string.empty": LangMsg.IS_NOT_ALLOWED_TO_BE_EMPTY
                })
            
        });
    }

     /**
    * Method: deleteProducts
    * Purpose: JOI validation
    * @param {*} LangMsg 
    */
      deleteProducts(LangMsg) {
        return Joi.object().keys({
            productId: Joi.string().required()
                .messages({
                    "any.required": LangMsg.IS_REQUIRED,
                    "string.empty": LangMsg.IS_NOT_ALLOWED_TO_BE_EMPTY
                })
            
        });
    }

}
module.exports = new ProductRequest();
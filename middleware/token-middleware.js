
const Constant = require('../config/constant');
const Message = require('../locale/message');
const CommonHelper = require('../util/common-helper');
const JwtHelper = require('../util/jwt-token-helper');


 Authorize = (req, res, next) => {
    try {
        const lang = req.headers.lang ? req.headers.lang : req.app.get("lang");
        req.app.set("lang", lang)
        const LangMsg = Message[req.app.get("lang")];
        /**
         * Authenticate user using Jwt token auth mechanism 
         */
        const accessToken = req.headers['access_token'] ? req.headers['access_token'] : (req.headers['access-token'] ? req.headers['access-token'] : '');
        req.headers['access_token'] = accessToken;
        if (!accessToken) {
            return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_401_UNAUTHORIZED, { "message": LangMsg.FAILED_TO_AUTHENTICATE });
        } else {
            JwtHelper.verify(accessToken).then((decode) => {
                req.user = decode;
                let permission = req.user.permission
                let perms = permission.split(',')
                var allow = false;
                perms.forEach(function(perm){
                    if (req.method == "POST" && perm == 'create')  {
                        allow = true
                    }
                    else if (req.method == "GET" && perm == 'fetch') {
                        allow = true
                    }
                    else if (req.method == "PUT" && perm == 'update')  {
                        allow = true
                    }
                    else if (req.method == "DELETE" && perm == 'delete')  {
                        allow = true
                    }
      
                })
                if (allow) return next();
                else return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_401_UNAUTHORIZED, { "message": LangMsg.ACCESS_DENIED });
                // return next();
            }).catch((err) => {
                switch (err.name) {
                    case 'TokenExpiredError':
                        return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_401_UNAUTHORIZED, { "message": LangMsg.TOKEN_EXPIRED });
                    default:
                        return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_500_INTERNAL_SERVER_ERROR, {
                            "message":
                                LangMsg.SOMETHING_WENT_WRONG
                        });
                }
            })
        }
        //-------------------------- END Check Cognito Authorize
    } catch (e) {
        return CommonHelper.sendError(res, Constant.STATUS_CODE.HTTP_500_INTERNAL_SERVER_ERROR, { "message": e.stack.toString() });
    }
}



module.exports = {
    Authorize
}